package biz.dev.db.trace.tool.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name="root" )
@XmlAccessorType( XmlAccessType.NONE )
public class TraceToolXmlConfiguration {

	@XmlElement( name="include" )
	private List<String> includes;
	
	@XmlElement( name="exclude" )
	private List<String> excludes;
	
	@XmlElement( name="log-line-start-pattern" )
	private String logLineStartPattern;
	
	@XmlElement( name="log-line-start-date-format" )
	private String logLineStartDateFormat;
	
	@XmlElement( name="log-line-start-date-length" )
	private int logLineStartDateLength;
	
	public TraceToolXmlConfiguration() {
		includes = new ArrayList<String>(1);
		excludes = new ArrayList<String>(1);
	}
	
	public List<String> getIncludes() {
		return includes;
	}
	
	public void setIncludes( List<String> includes ) {
		this.includes = includes;
	}
	
	public List<String> getExcludes() {
		return excludes;
	}
	
	public void setExcludes( List<String> excludes ) {
		this.excludes = excludes;
	}

	public String getLogLineStartPattern() {
		return logLineStartPattern;
	}
	
	public void setLogLineStartPattern(String logLineStartPattern) {
		this.logLineStartPattern = logLineStartPattern;
	}
	
	public String getLogLineStartDateFormat() {
		return logLineStartDateFormat;
	}
	
	public void setLogLineStartDateFormat(String logLineStartDateFormat) {
		this.logLineStartDateFormat = logLineStartDateFormat;
	}
	
	public int getLogLineStartDateLength() {
		return logLineStartDateLength;
	}
	
	public void setLogLineStartDateLength(int logLineStartDateLength) {
		this.logLineStartDateLength = logLineStartDateLength;
	}
	
	@Override
	public String toString() {
		return "TraceToolXmlConfiguration [includes=" + includes + ", excludes=" + excludes + "]";
	}
}