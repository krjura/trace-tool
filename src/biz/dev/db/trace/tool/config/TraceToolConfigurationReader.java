package biz.dev.db.trace.tool.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import biz.dev.db.trace.tool.utils.FileUtils;


public class TraceToolConfigurationReader {

	public static TraceToolXmlConfiguration readConfiguration( String configurationFile ) {
		InputStream in = null;
		
		try {
			in = getConfigurationInputStream( configurationFile );
		
			JAXBContext context = JAXBContext.newInstance( TraceToolXmlConfiguration.class );
			return ( TraceToolXmlConfiguration ) context.createUnmarshaller().unmarshal( in );
		} catch ( FileNotFoundException ex ) {
			throw new IllegalArgumentException( "Configuration file not found");
		} catch ( JAXBException ex ) {
			throw new IllegalArgumentException( "Invalid configuration file detected", ex );
		}
		
		finally {
			FileUtils.close( in );
		}
	}

	private static InputStream getConfigurationInputStream(String configurationFile) throws FileNotFoundException {
		if( configurationFile == null ) {
			System.out.println( "Using build-in configuration file");
			return TraceToolConfigurationReader.class.getResourceAsStream( "/trace-tool-configuration.xml");
		} 
		
		System.out.println( "Using configuration file: " + configurationFile);
		return new FileInputStream( new File( configurationFile ) );
	}
}
