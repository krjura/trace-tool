package biz.dev.db.trace.tool.utils;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeUtils {

	public static Calendar getCalendar( Date time ) {
		Calendar timeCalendar = Calendar.getInstance( TimeZone.getTimeZone( "UTC") );
		timeCalendar.setTime( time );
		return timeCalendar;
	}
}
