package biz.dev.db.trace.tool.utils;

import java.io.IOException;
import java.io.InputStream;

public class FileUtils {

	public static void close( InputStream is ) {
		if( is == null ) {
			return;
		}
		
		try {
			is.close();
		} catch (IOException e) {}
	}
}
