package biz.dev.db.trace.tool;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import biz.dev.db.trace.tool.config.TraceToolConfigurationReader;
import biz.dev.db.trace.tool.config.TraceToolXmlConfiguration;

public class LogParser {

	private List<File> inputFiles = new LinkedList<File>();
	private File outputFile = null;
	private String configurationFile = null;
	private String before = null;
	private String after = null;
	private boolean readAll = false;

	private TraceToolXmlConfiguration configuration;
	private TrafficPrinter printer;

	public LogParser(String[] args) throws IOException {
		parseArguments(args);
		validateArguments();
		
		configuration = TraceToolConfigurationReader.readConfiguration( configurationFile );
		printer = new TrafficPrinter( before, after, readAll, configuration );

		parse();
		close();
	}

	private void parse() throws IOException {
		for (File file : inputFiles) {
			if (!file.exists()) {
				throw new FileNotFoundException("Log file not found ");
			}

			LogLineReader reader = new LogLineReader( file, configuration );
			String next = null;
			
			try {
				while ((next = reader.next()) != null) {
					printer.collect(next);
				}
			} finally {
				reader.close();
			}
		}
	}

	private void close() throws IOException {
		printer.output(outputFile);
	}
	
	private void usage() {
		StringBuilder builder = new StringBuilder();
		builder.append( "The following options are available\n");
		builder.append( "--input <file> - Adds a input file for the scripts from which the logs lines are going to be read. This command can be repeated multiple times\n" );
		builder.append( "--output <file> - File where the trace will be written into\n" );
		builder.append( "--after yyyy-MM-dd/HH:mm:ss - Date in the specified format. Long lines after this date will be skipped\n");
		builder.append( "--before yyyy-MM-dd/HH:mm:ss - Date in the specified format. Long lines before this date will be skipped\n");
		builder.append( "--read-all - Does not match predefined include and exclude patters\n");
		builder.append( "--configuration <file> - Trace Tool Configuration \n");
		builder.append( "--usage - Prints this help\n");
		
		System.out.print( builder.toString() );
	}
	
	private void parseArguments(String[] args) {
		if (args.length == 0) {
			throw new IllegalArgumentException("Please specify arguments");
		}

		// parse arguments
		for (int i = 0, n = args.length; i < n; i++) {
			String param = getArgumentValue( args, i );

			if (param.equals("--input")) {
				String file = getArgumentValue( args, ++i );
				inputFiles.add(new File(file));
			} else if (param.equals("--output")) {
				String file = getArgumentValue( args, ++i );
				outputFile = new File(file);
			} else if (param.equals("--before")) {
				before = getArgumentValue( args, ++i );
			} else if (param.equals("--after")) {
				after = getArgumentValue( args, ++i );
			} else if( param.equals("--read-all") ) {
				readAll = true;
			} else if( param.equals( "--configuration") ) {
				configurationFile = getArgumentValue( args, ++i );
			} else if( param.equals("--usage") ) {
				usage();
				System.exit(0);
			}
		}
	}
	
	private String getArgumentValue( String[] args, int i ) {
		if( args.length <= i ) {
			throw new IllegalArgumentException("Incomplete argument specification");
		}
		
		return args[i];
	}
	
	private void validateArguments() {
		// check arguments
		if (inputFiles.size() == 0) {
			throw new IllegalArgumentException("Please define input file(s)");
		}

		if (outputFile == null) {
			throw new IllegalArgumentException("Please define output file");
		}
	}

	public static void main(String[] args) throws IOException {
		new LogParser( args );
	}
}
