package biz.dev.db.trace.tool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

import biz.dev.db.trace.tool.config.TraceToolXmlConfiguration;

public class LogLineReader {

	private BufferedReader lineReader;
	
	private String nextLine = null;
	
	private Pattern logLineStartPattern = null;
	
	public LogLineReader( File file, TraceToolXmlConfiguration configuration ) throws FileNotFoundException {
		setLogLineStartPattern( configuration );
		
		lineReader = new BufferedReader( new FileReader( file ) );
	}

	private void setLogLineStartPattern( TraceToolXmlConfiguration configuration ) {
		if( configuration.getLogLineStartPattern() == null || configuration.getLogLineStartPattern().equals( "" ) ) {
			throw new IllegalArgumentException( "Log line start pattern not defined");
		}
		
		logLineStartPattern = Pattern.compile( configuration.getLogLineStartPattern() );
	}
	
	public String next() throws IOException {
		StringBuilder nextLogEntry = new StringBuilder();
		
		if( nextLine == null ) {
			nextLine = lineReader.readLine();
		}
		
		if( nextLine == null ) {
			return null;
		}
		
		if( logLineStartPattern.matcher( nextLine ).matches() ) {
			// found line now get all with new line
			nextLogEntry.append( nextLine + "\n" );
			nextLine = null; 
			
			appendAllTillNextLogLine( nextLogEntry );
			return nextLogEntry.toString();
		}
		
		return null;
	}

	private void appendAllTillNextLogLine(StringBuilder nextLogEntry ) throws IOException {
		while( ( nextLine = lineReader.readLine()  ) != null && !logLineStartPattern.matcher( nextLine ).matches() ) {
			nextLogEntry.append( nextLine + "\n" );
		}
	}
	
	public void close() {
		try {
			lineReader.close();
		} catch ( Exception ex ) {}
	}
}
