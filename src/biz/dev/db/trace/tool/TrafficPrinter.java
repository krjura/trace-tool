package biz.dev.db.trace.tool;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import biz.dev.db.trace.tool.config.TraceToolXmlConfiguration;
import biz.dev.db.trace.tool.utils.TimeUtils;

public class TrafficPrinter {
	
	private List<LogRecord> timeline;
	
	private String BEFORE_DATE_FORMAT = "yyyy-MM-dd/HH:mm:ss";
	private SimpleDateFormat beforeFormat = new SimpleDateFormat( BEFORE_DATE_FORMAT );
	private String AFTER_DATE_FORMAT = "yyyy-MM-dd/HH:mm:ss";
	private SimpleDateFormat afterFormat = new SimpleDateFormat( AFTER_DATE_FORMAT );
	private boolean readAll;
	
	private TraceToolXmlConfiguration configuration;
	
	private Calendar beforeTimestamp;
	private Calendar afterTimestamp;
	
	public TrafficPrinter( String before, String after, boolean readAll, TraceToolXmlConfiguration configuration ) {
		this.timeline = new LinkedList<LogRecord>();
		this.readAll = readAll;
		this.configuration = configuration;
		
		parseBefore( before );
		parseAfter( after );
	}
	
	private void parseBefore( String before )  {
		if( before == null ) {
			return;
		}
		
		try {
			this.beforeTimestamp =  TimeUtils.getCalendar( beforeFormat.parse( before ) );
		} catch ( ParseException ex ) {
			throw new IllegalArgumentException( "Before in invalid format. Expecting " + BEFORE_DATE_FORMAT );
		}
	}
	
	private void parseAfter( String after )  {
		if( after == null ) {
			return;
		}
		
		try {
			this.afterTimestamp = TimeUtils.getCalendar( afterFormat.parse( after ) );
		} catch ( ParseException ex ) {
			throw new IllegalArgumentException( "after in invalid format. Expecting " + AFTER_DATE_FORMAT );
		}
	}
	
	public void collect( String record ) {
		LogRecord logRecord = new LogRecord( record, configuration );
		
		if( beforeTimestamp != null && logRecord.getTimestamp().after( beforeTimestamp ) ) {
			return;
		} else if(  afterTimestamp != null && logRecord.getTimestamp().before( afterTimestamp ) ) {
			return;
		}
		
		if( readAll ) {
			this.timeline.add( logRecord );
			return;
		}
		
		if( isIncluded( logRecord.getRecord() ) && !isExcluded( logRecord.getRecord() ) ) {
			this.timeline.add( logRecord );
		}
	}
	
	private boolean isExcluded(  String logRecord ) {
		for( String pattern : configuration.getExcludes() ) {
			if( logRecord.contains( pattern ) ) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean isIncluded( String logRecord ) {
		for( String pattern : configuration.getIncludes() ) {
			if( logRecord.contains( pattern ) ) {
				return true;
			}
		}
		
		return false;
	}
	
	public void print() {
		Collections.sort( timeline );
		
		for( LogRecord record : timeline ) {
			System.out.println( record.getRecord() );
		}
	}
	
	public void output( File outputFile ) throws IOException {
		Collections.sort( timeline );
		
		FileOutputStream fos = new FileOutputStream( outputFile );
		
		try  {
			for( LogRecord record : timeline ) {
				fos.write( record.getRecord().getBytes() );
			}
			fos.flush();
		} finally {
			fos.close();
		}
	}
}