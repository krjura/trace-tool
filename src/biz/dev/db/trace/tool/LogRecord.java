package biz.dev.db.trace.tool;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import biz.dev.db.trace.tool.config.TraceToolXmlConfiguration;
import biz.dev.db.trace.tool.utils.TimeUtils;

public class LogRecord implements Comparable<LogRecord> {

	private Calendar timestamp;
	
	private String record;
	
	private TraceToolXmlConfiguration configuration;
	
	private SimpleDateFormat dateFormat = null;
	
	public LogRecord( String record, TraceToolXmlConfiguration configuration ) {
		if( configuration.getLogLineStartDateFormat() == null ) {
			throw new IllegalArgumentException( "Log line start date format not defined" ); 
		}
		
		this.configuration = configuration;
		this.dateFormat = new SimpleDateFormat( configuration.getLogLineStartDateFormat() );
		this.timestamp = TimeUtils.getCalendar( parseTimestamp( record ) );
		
		this.record = record;
	}

	private Date parseTimestamp(String line)  {
		try {
			return dateFormat.parse( line.substring( 0, configuration.getLogLineStartDateLength() ) );
		} catch ( ParseException ex ) {
			return new Date();
		}
	}
	
	public String getRecord() {
		return record;
	}

	@Override
	public int compareTo( LogRecord o ) {
		if( o == null ) {
			return -1;
		}
		
		if( o.timestamp == null ) {
			return -1;
		}
		
		return this.timestamp.compareTo( o.timestamp );
	}
	
	public Calendar getTimestamp() {
		return timestamp;
	}
}
