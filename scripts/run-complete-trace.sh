#!/bin/sh

SYSTEM_INPUT_SPEC="--input scp21/system.log --input scp22/system.log"
SYSTEM_OUTPUT_SPEC="--output complete-trace-system.log"

NGIN_INPUT_SPEC="--input scp21/ngin.log --input scp22/ngin.log"
NGIN_OUTPUT_SPEC="--output complete-trace-ngin.log"

DATE_SPEC="--after 2012-07-25/15:46:42 --before 2012-07-25/15:47:11"

CONFIGURATION_FILE="--configuration trace-tool-configuration.xml"

# SYSTEM
java -jar libs/trace-tool.jar $SYSTEM_INPUT_SPEC $SYSTEM_OUTPUT_SPEC $DATE_SPEC --read-all $CONFIGURATION_FILE
# NGIN
java -jar libs/trace-tool.jar $NGIN_INPUT_SPEC $NGIN_OUTPUT_SPEC $DATE_SPEC --read-all $CONFIGURATION_FILE
