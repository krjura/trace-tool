#!/bin/sh

INPUT_SPEC="--input scp21/system.log --input scp22/system.log"
OUTPUT_SPEC="--output complete-trace-system.log"
CONFIGURATION_FILE="--configuration trace-tool-configuration.xml"

java -jar libs/trace-tool.jar $INPUT_SPEC $OUTPUT_SPEC --after 2012-07-25/15:46:42 --before 2012-07-25/15:47:11 --read-all $CONFIGURATION_FILE
